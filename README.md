This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Getting Started

First, run the development server:

```bash
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `app/page.tsx`. The page auto-updates as you edit the file.

This project uses [`next/font`](https://nextjs.org/docs/basic-features/font-optimization) to automatically optimize and load Inter, a custom Google Font.

## Using Docker

### Requirement

Docker installed on your local machine: [documentation](https://docs.docker.com/get-docker/)

First step

```bash
yarn build
```

### Development environment - for doing testing

#### To build

```
docker-compose up --build
```

#### To start

```
docker-compose up
```

Open http://localhost:3000

### Production environment - for users

#### To build

```
docker-compose -f docker-compose.production.yml up --build

```

#### To start

```
docker-compose -f docker-compose.production.yml up
```

Open http://localhost:8080

### To stop

```
docker-compose down
```
