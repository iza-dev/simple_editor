import { useEffect, useState } from "react";

class PubSub {
    subscribers: any;
    constructor() {
        this.subscribers = {};
    }

    subscribe(event: any, callback: any) {
        if (!this.subscribers[event]) {
            this.subscribers[event] = [];
        }
        this.subscribers[event].push(callback);
    }

    publish(event: any, data: any) {
        if (!this.subscribers[event]) return;
        this.subscribers[event].forEach((callback: any) => callback(data));
    }

    unsubscribe(event: any, callback: any) {
        if (!this.subscribers[event]) return;
        this.subscribers[event] = this.subscribers[event].filter(
            (subscriber: any) => subscriber !== callback
        );
    }
}

export const statePubSub = new PubSub();

export type PlaneT = { id: number; width: number; height: number };

const initState: {
    selectedPlane: number | undefined;
    planes: PlaneT[];
    history: Array<{ selectedPlane: number | undefined; planes: PlaneT[] }>;
    historyIndex: number;
} = {
    selectedPlane: undefined,
    planes: [{ id: 0, width: 1, height: 1 }, { id: 1, width: 1, height: 1 }],
    history: [],
    historyIndex: -1,
};

type StateType = typeof initState;

function undo(state: StateType): StateType {
    const newHistoryIndex = Math.max(state.historyIndex - 1, 0);

    return {
        ...state.history[newHistoryIndex],
        history: state.history,
        historyIndex: newHistoryIndex,
    };
}

function redo(state: StateType): StateType {
    const newHistoryIndex = Math.min(state.historyIndex + 1, state.history.length - 1);

    return {
        ...state.history[newHistoryIndex],
        history: state.history,
        historyIndex: newHistoryIndex,
    };
}

export function createStateHook(initialValue: StateType) {
    return () => {
        const [value, setValue] = useState<StateType>(initialValue);

        useEffect(() => {
            return statePubSub.subscribe('onChangePlane', setValue);
        }, []);

        const updateValue = (v: Partial<StateType>) => {
            setValue((prevValue: StateType) => {
                const newState = {
                    ...prevValue,
                    ...v,
                    historyIndex: prevValue.historyIndex + 1,
                };
                const newHistory = [
                    ...prevValue.history.slice(0, newState.historyIndex),
                    {
                        selectedPlane: newState.selectedPlane,
                        planes: newState.planes,
                    },
                ];
                const updatedState = {
                    ...newState,
                    history: newHistory,
                };
                statePubSub.publish('onChangePlane', updatedState);
                return updatedState;
            });
        };

        const undoValue = () => {
            setValue((prevValue: StateType) => {
                const newState = undo(prevValue);
                statePubSub.publish('onChangePlane', newState);
                return newState;
            });
        };

        const redoValue = () => {
            setValue((prevValue: StateType) => {
                const newState = redo(prevValue);
                statePubSub.publish('onChangePlane', newState);
                return newState;
            });
        };

        return [value, updateValue, undoValue, redoValue] as const;
    };
}

export const useGlobalState = createStateHook(initState);
