'use client';

import { Box, Flex, Text } from '@chakra-ui/react';
import { FC } from 'react';

import Panel from '@/components/panel';
import Plane from '@/components/plane';

const Home: FC = () => {
  const mode_dev = process.env.ENV_MODE;
  return (
    <main>
      <Flex h={'100vh'} w={'100vw'}>
        <Box
          w={'calc(100vw - 250px)'}
          backgroundColor={mode_dev === 'development' ? 'pink' : 'cyan'}
          textAlign={'center'}
        >
          <Text textTransform={'capitalize'}>{mode_dev}</Text>
          <Plane />
        </Box>
        <Box w={250}>
          <Panel />
        </Box>
      </Flex>
    </main>
  );
};

export default Home;
