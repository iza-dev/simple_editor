import { PlaneT, useGlobalState } from '@/globalState';
import { Box, Flex, Text } from '@chakra-ui/react';
import { FC } from 'react';
import SliderInput from './SliderInput';

type PlaneProperty = keyof PlaneT;

const Panel: FC = () => {
  const [state, setState] = useGlobalState();

  const onSliderChange = (property: PlaneProperty, value: number) => {
    if (state.selectedPlane != undefined) {
      const selectedIndex = state.planes.findIndex(plane => plane.id === state.selectedPlane);
      if (selectedIndex !== -1) {
        const newPlane = { ...state.planes[state.selectedPlane] };
        newPlane[property] = value + 1;
        const newPlanes = {
          ...state,
          planes: state.planes.map((plane, index) =>
            index === selectedIndex ? { ...plane, ...newPlane } : plane
          )
        };
        setState(newPlanes);
      }
    }
  };

  const initialWidth = state.planes[state.selectedPlane || 0].width - 1;
  const initialHeight = state.planes[state.selectedPlane || 0].height - 1;

  return (
    <Flex direction={'column'} gap={'2rem'} padding={'2rem'}>
      <Box>
        <Text>Width</Text>
        <SliderInput
          value={initialWidth}
          onChange={(value: number) => onSliderChange('width', value)}
          children={initialWidth}
        />
      </Box>
      <Box>
        <Text>Height</Text>
        <SliderInput
          value={initialHeight}
          onChange={(value: number) => onSliderChange('height', value)}
          children={initialHeight}
        />
      </Box>
    </Flex>
  );
};

export default Panel;
