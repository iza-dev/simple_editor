import { Slider, SliderFilledTrack, SliderThumb, SliderTrack } from '@chakra-ui/react';
import { FC } from 'react';

const SliderInput: FC<{ value: number; onChange: (value: any) => void; children: number }> = ({
  value,
  onChange,
  children
}) => {
  return (
    <Slider
      defaultValue={0}
      min={0}
      max={100}
      step={1}
      flex="1"
      focusThumbOnChange={false}
      value={value}
      onChange={onChange}
    >
      <SliderTrack>
        <SliderFilledTrack />
      </SliderTrack>
      <SliderThumb fontSize="sm" boxSize="32px" children={children} />
    </Slider>
  );
};

export default SliderInput;
