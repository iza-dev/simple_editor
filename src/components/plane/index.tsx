'use client';

import { useGlobalState } from '@/globalState';
import { Box, Flex, IconButton } from '@chakra-ui/react';
import { OrbitControls } from '@react-three/drei';
import { Canvas, MeshProps } from '@react-three/fiber';
import { FC, Suspense, memo, useEffect, useMemo, useRef } from 'react';
import { LuRedo, LuUndo } from 'react-icons/lu';
import { DoubleSide, Mesh, RepeatWrapping, TextureLoader } from 'three';

const Index: FC = () => {
  const [state, setState, undoState, redoState] = useGlobalState();

  const enableCameraControl = process.env.ENV_MODE === 'development';

  return (
    <Box h={'100%'}>
      <Flex gap={5} justifyContent={'center'} marginTop={5}>
        <IconButton
          onClick={() => {
            if (state.historyIndex > 0) undoState();
          }}
          icon={<LuUndo />}
          aria-label={'button for undo'}
        />
        <IconButton
          onClick={() => {
            if (state.historyIndex > 0) redoState();
          }}
          icon={<LuRedo />}
          aria-label={'button for redo'}
        />
      </Flex>
      <Canvas flat linear>
        <ambientLight intensity={2} />
        <pointLight position={[40, 40, 40]} />
        {enableCameraControl && <OrbitControls />}
        <Suspense fallback={null}>
          <Plane
            position={[-0.1 + state.planes[0].width / -2, 0, 0]}
            width={state.planes[0].width}
            height={state.planes[0].height}
            isSelected={state.selectedPlane === 0}
            onClick={event => {
              setState({ ...state, selectedPlane: 0 });
              event.stopPropagation();
            }}
            onPointerMissed={() => setState({ ...state, selectedPlane: undefined })}
          />
          <Plane
            position={[0.1 + state.planes[1].width / 2, 0, 0]}
            width={state.planes[1].width}
            height={state.planes[1].height}
            isSelected={state.selectedPlane === 1}
            onClick={event => {
              setState({ ...state, selectedPlane: 1 });
              event.stopPropagation();
            }}
            onPointerMissed={() => setState({ ...state, selectedPlane: undefined })}
          />
        </Suspense>
      </Canvas>
    </Box>
  );
};

const Plane: FC<MeshProps & { isSelected?: boolean; width: number; height: number }> = memo(
  ({ width, height, isSelected, ...props }) => {
    const meshRef = useRef<Mesh>(null);

    const texture = useMemo(() => {
      const loadedTexture = new TextureLoader().load('/wood_texture.jpg');
      loadedTexture.repeat.set(0.5 * width, 0.5);
      loadedTexture.wrapS = loadedTexture.wrapT = RepeatWrapping;
      loadedTexture.needsUpdate = true;
      return loadedTexture;
    }, [width]);

    useEffect(() => {
      return () => {
        texture.dispose();
      };
    }, [texture]);

    return (
      <mesh {...props} ref={meshRef}>
        <planeGeometry args={[width, height, 1]} />
        {texture && (
          <meshPhongMaterial
            attach="material"
            color={isSelected ? 'green' : 'red'}
            map={texture}
            side={DoubleSide}
          />
        )}
      </mesh>
    );
  }
);

export default Index;
