/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: true,
  output: 'export',
  env: {
    ENV_MODE: process.env.ENV_MODE
  }
};

module.exports = nextConfig;
